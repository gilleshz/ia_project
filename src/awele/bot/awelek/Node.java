package awele.bot.awelek;

import awele.core.Board;

public class Node {

    private Board board; // le plateau du jeu d'Awélé
    private boolean maxNode; // booléen définissant le type de noeud (true: noeud max, false: noeud min)
    private int maximizingPlayerScore; // score du joueur maximisant
    private int minimizingPlayerScore; // score du joueur minimisant
    private int maximizingPlayerPreviousMove; // précédent coup joué par le joueur maximisant (entier entre 0 et Board.NB_HOLES - 1)
    private boolean root; // vaut true si le noeud est à la racine de l'arbre

    /**
     * Constructeur de racine
     * @param board le plateau du jeu d'Awélé
     */
    Node(Board board) {
        this.board = board;
        this.maximizingPlayerScore = 0;
        this.minimizingPlayerScore = 0;
        this.root = true;
        this.maxNode = true;
    }

    /**
     * Constructeur de noeuds
     * @param board le plateau du jeu d'Awélé
     * @param maxNode booléen définissant le type de noeud (true: noeud max, false: noeud min)
     * @param maximizingPlayerScore score du joueur maximisant
     * @param minimizingPlayerScore score du joueur minimisant
     * @param maximizingPlayerPreviousMove précédent coup joué par le joueur maximisant (entier entre 0 et Board.NB_HOLES - 1)
     */
    Node(Board board, boolean maxNode, int maximizingPlayerScore, int minimizingPlayerScore, int maximizingPlayerPreviousMove) {
        this.board = board;
        this.maximizingPlayerScore = maximizingPlayerScore;
        this.minimizingPlayerScore = minimizingPlayerScore;
        this.root = false;
        this.maxNode = maxNode;
        this.maximizingPlayerPreviousMove = maximizingPlayerPreviousMove;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    boolean isMaxNode() {
        return maxNode;
    }

    int getMaximizingPlayerScore() {
        return maximizingPlayerScore;
    }

    int getMinimizingPlayerScore() {
        return minimizingPlayerScore;
    }

    int getMaximizingPlayerPreviousMove() {
        return maximizingPlayerPreviousMove;
    }

    boolean isRoot() {
        return root;
    }

    /**
     * @return le score du bot
     */
    int getPlayerScore() {
        return this.getMaximizingPlayerScore();
    }

    /**
     * @return le score de l'adversaire
     */
    int getOpponentScore() {
        return this.getMinimizingPlayerScore();
    }

    /**
     * @return le précédent coup joué par le bot
     */
    int getPreviousMove() {
        return this.getMaximizingPlayerPreviousMove();
    }
}
