# Awelek

Awelek est un bot capable de jouer à l'Awélé. Il utilise l'algorithme MinMax avec un élagage Alpha-Beta.

## En cas de lenteur

Si l'exécution du bot est trop longue, décrémenter la constante DEPTH de la classe Awelek (Awelek.java). 