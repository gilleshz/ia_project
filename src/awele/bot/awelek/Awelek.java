package awele.bot.awelek;

import awele.bot.Bot;
import awele.core.Board;
import awele.core.InvalidBotException;

public class Awelek extends Bot {

    /** Algorithme Alpha-Beta */
    private AlphaBeta alphaBeta;
    private static final int DEPTH = 8;

    /** Algorithme Knn2 */
    private Knn2 knn2;

    /** Apprentissage effectué */
    private boolean hasLearned = false;

    /**
     * Constructeur
     * @throws InvalidBotException si le bot n'est pas valide
     */
    public Awelek() throws InvalidBotException
    {
        this.setBotName ("Awelek");
        this.addAuthor ("Gilles Heintz");
        this.addAuthor ("Jules Charron");

        this.alphaBeta = new AlphaBeta(DEPTH);
        this.knn2 = new Knn2();
    }

    /**
     *  Rien à faire
     */
    @Override
    public void initialize() {}

    /**
     * Apprentissage du bot
     * Cette fonction est appelée une fois au chargement du bot
     */
    @Override
    public void learn() {
        if (!this.hasLearned) {
            this.knn2.learn();
            this.hasLearned = true;
        }
    }

    @Override
    public double[] getDecision(Board board) {

        // On effectue l'apprentissage du bot (au cas où cela n'a pas été fait préalablement)
        this.learn();

        // On fait appel à alphaBeta pour obtenir son tableau de décision
        double [] decision = this.alphaBeta.getDecision(board);

        // On compte le nombre de fois que la valeur maximale apparaît dans le tableau de décision
        double max = Double.NEGATIVE_INFINITY;
        int countMax = 0;
        for (double val : decision) {
            if (val > max) {
                max = val;
                countMax = 1;
            } else if (val == max) {
                countMax++;
            }
        }

        // Si plusieurs coups ont la valeur maximale (i.e. on a une incertitude sur le coup à jouer)
        if (countMax > 1) {
            // Alors on fait appel à l'algorithme knn2 pour tenter de départager
            double [] knn2Decision = this.knn2.getDecision(board);

            // On cherche la valeur maximum de knn2Decision sur les index de max
            double knn2Max = Double.NEGATIVE_INFINITY;
            for (int i = 0; i < decision.length; i++) {
                if (decision[i] == max && knn2Decision[i] > knn2Max) {
                    knn2Max = knn2Decision[i];
                }
            }

            // Si au moins une des valeurs de knn2 correspondante est positive
            // (Ce test permet de s'assurer qu'on ne modifie pas le résultat d'AlphaBeta)
            if (knn2Max >= 0) {
                // Alors on additionne les valeurs de knn2 aux maximums de la decision sans toucher aux autres valeurs
                for (int i = 0; i < decision.length; i++) {
                    if (decision[i] == max) {
                        decision[i] += knn2Decision[i];
                    }
                }
            }
        }

        return decision;
    }
}
