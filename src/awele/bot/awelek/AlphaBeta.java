package awele.bot.awelek;

import awele.core.Board;

public class AlphaBeta {

    private int botPlayer;
    private int opponentPlayer;
    private double [] decision;
    private int depth;
    private static final int DEFAULT_DEPTH = 6;

    /** Poids des heuristiques */
    private static final double W1 = 0.198649;
    private static final double W2 = 0.190084;
    private static final double W3 = 0.370793;
    private static final double W4 = 1.0;
    private static final double W5 = 0.418841;
    private static final double W6 = 0.565937;

    /**
     * Constructeur par défaut
     */
    public AlphaBeta() {
        this.initialize();
        this.depth = DEFAULT_DEPTH;
    }

    /**
     * Constructeur avec paramétrisation de la profondeur maximale
     * @param depth profondeur maximale
     */
    public AlphaBeta(int depth) {
        this.initialize();
        this.depth = depth;
    }

    private void initialize()
    {
        this.decision = new double[Board.NB_HOLES];
    }


    double[] getDecision(Board board) {

        // On fixe les joueurs pour le plateau donné
        this.botPlayer = board.getCurrentPlayer();
        this.opponentPlayer = Board.otherPlayer(this.botPlayer);

        // Appel à la fonction récursive alphaBeta
        this.alphaBeta(new Node(board), this.depth, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);

        return this.decision;
    }

    /**
     * alphaBeta est une méthode récursive
     * son résultat final est enregistré dans l'attribut de classe this.decision
     *
     * @param node Le noeud a évaluer
     * @param depth La profondeur maximale
     * @param alpha
     * @param beta
     * @return la valeur du noeud
     */
    private double alphaBeta(Node node, int depth, double alpha, double beta) {

        // Identification du joueur
        int player = node.isMaxNode() ? this.botPlayer : this.opponentPlayer;

        // Récupération du plateau
        Board board = node.getBoard();

        // Conditions d'arrêt :
        //    - Profondeur maximale atteinte
        //    - Aucun coup valide
        //    - Moins de 6 graines sur le plateau
        if (depth == 0 || !this.playerHasValidMoves(board, player) || board.getNbSeeds() < 6) {
            return enhancedEvaluation(node);
        }

        int score;
        double[] decision;

        if (node.isMaxNode()) {
            double maxEval = Double.NEGATIVE_INFINITY;
            boolean [] validMoves = board.validMoves(player);

            // Parcours des coups valides
            for (int i = 0; i < validMoves.length; i++){
                if (validMoves[i]) {
                    // Simulation du coup courant
                    decision = this.decisionSimulation(i);
                    Board childBoard = board.playMoveSimulationBoard(player, decision);
                    score = board.playMoveSimulationScore(player, decision);

                    // Création du noeud fils
                    Node child = new Node(
                            childBoard,
                            false,
                            node.getMaximizingPlayerScore() + score,
                            node.getMinimizingPlayerScore(),
                            i
                    );

                    // Appel récursif sur le fils
                    double eval = alphaBeta(child, depth - 1, alpha, beta);

                    maxEval = Math.max(maxEval, eval);

                    // Si l'on est à la racine de l'arbre
                    // alors on enregistre l'évaluation dans le tableau de décision
                    if (node.isRoot()) {
                        this.decision[i] = maxEval;
                    }

                    // Élagage alpha-beta
                    alpha = Math.max(alpha, eval);
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
            return maxEval;

        } else {
            double minEval = Double.POSITIVE_INFINITY;
            boolean [] validMoves = board.validMoves(player);

            // Parcours des coups valides
            for (int i = 0; i < validMoves.length; i++){
                if (validMoves[i]) {
                    // Simulation du coup courant
                    decision = this.decisionSimulation(i);
                    Board childBoard = board.playMoveSimulationBoard(player, decision);
                    score = board.playMoveSimulationScore(player, decision);

                    // Création du noeud fils
                    Node child = new Node(
                            childBoard,
                            true,
                            node.getMaximizingPlayerScore(),
                            node.getMinimizingPlayerScore() + score,
                            node.getMaximizingPlayerPreviousMove()
                    );

                    // Appel récursif sur le fils
                    double eval = alphaBeta(child, depth - 1, alpha, beta);

                    // Élagage alpha-beta
                    minEval = Math.min(minEval, eval);
                    beta = Math.min(beta, eval);
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
            return minEval;

        }
    }

    /**
     * Évaluation améliorée du plateau de jeu basée sur les travaux présentés dans l'article suivant :
     * Exploration and analysis of the evolution of strategies for Mancala variants.
     * Divilly, C., O’Riordan, C., & Hill, S. (2013).
     * 2013 IEEE Conference on Computational Inteligence in Games (CIG).
     * doi:10.1109/cig.2013.6633628
     *
     * @param node le noeud à évaluer
     * @return l'évaluation du noeud
     */
    private double enhancedEvaluation(Node node) {

        // Récupération du plateau
        Board board = node.getBoard();

        // Récupération de l'état des trous du joueur
        int[] playerHoles = board.getCurrentPlayer() == this.botPlayer ? board.getPlayerHoles() : board.getOpponentHoles();
        int playerSeeds = board.getCurrentPlayer() == this.botPlayer ? board.getPlayerSeeds() : board.getOpponentSeeds();

        /* Calcul des heuristiques */
        double h1 = (double) playerHoles[0]; // nombre de graines dans le trou le plus à gauche.
        double h2 = (double) playerSeeds; // nombre total de graines du côté du bot.
        double h3 = (double) numberOfNonEmptyHoles(playerHoles); // nombre de trous non vides du côté du bot.
        double h4 = (double) node.getPlayerScore(); // score du bot.
        double h5 = isRightmostMove(node.getPreviousMove()) ? 1.0 : 0.0; // le coup précédent est-il le plus à droite ?
        double h6 = (double) node.getOpponentScore(); // score de l’adversaire.

        /* Calcul de la combinaison pondérée des heuristiques */
        return h1 * W1 + h2 * W2 + h3 * W3 + h4 * W4 + h5 * W5 - h6 * W6;
    }

    /**
     * Compte le nombre de trous non vides dans la liste de trous donnés
     * @param holes liste de trous à vérifier
     * @return le nombre de trous non vides
     */
    private int numberOfNonEmptyHoles(int[] holes) {
        int result = 0;
        for (int hole : holes) {
            if (hole > 0) {
                result++;
            }
        }

        return result;
    }

    /**
     * Vérifie si le coup donné est le coup le plus à droite du plateau
     * @param move indice du coup à vérifier (nombre entier compris entre 0 et Board.NB_HOLES - 1)
     * @return true si le coup donné est le coup le plus à droite du plateau, false sinon
     */
    private boolean isRightmostMove(int move) {
        return move + 1 == Board.NB_HOLES;
    }

    /**
     * Vérifie si le joueur a des coups valides à jouer
     * @param board le plateau à tester
     * @return boolean
     */
    private boolean playerHasValidMoves(Board board, int player) {
        boolean [] validMoves = board.validMoves(player);
        for (boolean validMove : validMoves) {
            if (validMove) {
                return true;
            }
        }
        return false;
    }

    /**
     * Crée un tableau de décision pour simuler un coup
     * Les valeurs sont 0 partout et 1 à la position donée en paramètre (qui sera donc le coup joué)
     * @param position coup à jouer
     * @return tableau de décision
     */
    private double [] decisionSimulation(int position) {
        if (position < 0 || position >= Board.NB_HOLES) {
            throw new IndexOutOfBoundsException("La position doit être à l'intérieur du plateau");
        }
        double [] simulationDecision = new double[Board.NB_HOLES];
        for (int i = 0; i < simulationDecision.length; i++) {
            simulationDecision[i] = (i == position) ? 1 : 0;
        }
        return simulationDecision;
    }
}
