package awele.bot.awelek;

import awele.core.Board;
import awele.data.AweleData;
import awele.data.AweleObservation;

import java.util.HashMap;

/**
 * @deprecated n'est plus utilisé par le bot
 * Ancienne classe représentant les données qui étaient utilisées par la fonction d'évaluation d'AlphaBeta
 */
public class EvaluationData
{
    public static final int UNKNOWN = 0;
    public static final int WIN = 1;
    public static final int LOSS = -1;

    /**
     *  Données contenant l'identifiant du plateau, et le résultat correspondant (victoire ou défaite)
     */
    private HashMap<String, Boolean> hashData;

    /**
     *  Constructeur
     *  Extrait les données de la classe singleton AweleData pour les organiser dans la table hashData
     */
    public EvaluationData()
    {
        this.hashData = new HashMap<>();
        AweleData data = AweleData.getInstance ();

        for (AweleObservation observation: data) {
            String boardId = generateBoardId(observation.getPlayerHoles(), observation.getOppenentHoles());
            this.hashData.put(boardId, observation.isWon());

            // Ajout du plateau retourné (joueur et adversaire échangés + résultat inversé)
            boardId = generateBoardId(observation.getOppenentHoles(), observation.getPlayerHoles());
            this.hashData.put(boardId, !observation.isWon());
        }
    }

    /**
     * Donne le résultat de la partie en base de donnée correspondante au plateau donné
     * @param board le plateau à tester
     * @return le résultat de la partie (UNKNOWN, WIN, ou LOSS)
     */
    public int getResultFromBoard(Board board) {

        String boardId = generateBoardId(board.getPlayerHoles(), board.getOpponentHoles());

        if (hashData.containsKey(boardId)) {
            return this.hashData.get(boardId) ? EvaluationData.WIN : EvaluationData.LOSS;
        }

        return EvaluationData.UNKNOWN;
    }

    /**
     * Génère un identifiant unique correspondant à la configuration d'un plateau
     * @param playerHoles le nombre de graines dans les trous du joueur
     * @param opponentHoles le nombre de graines dans les trois de l'adversaire
     * @return le hashcode généré
     */
    private static String generateBoardId(int[] playerHoles, int[] opponentHoles) {

        StringBuilder result = new StringBuilder();

        for (int playerHole : playerHoles) {
            result.append(playerHole);
        }
        for (int opponentHole : opponentHoles) {
            result.append(opponentHole);
        }

        return result.toString();
    }
}
